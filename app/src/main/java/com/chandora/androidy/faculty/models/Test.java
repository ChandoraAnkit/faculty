package com.chandora.androidy.faculty.models;

import com.google.firebase.database.PropertyName;

/**
 * Created by Kalpesh Chandora on 1/10/18.
 */
public class Test {
    private String yearName;
    private String creationDate;
    private boolean isEnabled;

    @PropertyName("year_name")
    public String getYearName() {
        return yearName;
    }

    @PropertyName("year_name")
    public void setYearName(String yearName) {
        this.yearName = yearName;
    }

    @PropertyName("creation_date")
    public String getCreationDate() {
        return creationDate;
    }

    @PropertyName("creation_date")
    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    @PropertyName("is_enabled")
    public boolean isEnabled() {
        return isEnabled;
    }

    @PropertyName("is_enabled")
    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }


}
