package com.chandora.androidy.faculty.models;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.PropertyName;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Kalpesh Chandora on 30/9/18.
 */
public class TeachersListModel {

    private String teacherId;
    private boolean isClassTeacher;

    @PropertyName("teacher_id")
    public String getTeacherId() {
        return teacherId;
    }

    @PropertyName("teacher_id")
    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    @PropertyName("is_class_teacher")
    public boolean isClassTeacher() {
        return isClassTeacher;
    }

    @PropertyName("is_class_teacher")
    public void setClassTeacher(boolean classTeacher) {
        isClassTeacher = classTeacher;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("teacher_id", getTeacherId());
        hashMap.put("is_class_teacher", isClassTeacher());
        return hashMap;
    }

}
