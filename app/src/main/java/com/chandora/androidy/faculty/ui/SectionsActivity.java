package com.chandora.androidy.faculty.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.chandora.androidy.faculty.R;
import com.chandora.androidy.faculty.adapters.SectionListAdapter;
import com.chandora.androidy.faculty.models.newmodel.ClassTeacherModel;
import com.chandora.androidy.faculty.models.newmodel.SectionModel;
import com.chandora.androidy.faculty.models.newmodel.YearModel;
import com.chandora.androidy.faculty.utils.AppConstants;
import com.chandora.androidy.faculty.utils.AppPrefs;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SectionsActivity extends AppCompatActivity {

    private static final String TAG = "SectionsActivity";
    private DatabaseReference mRef;
    private AppPrefs appPrefs;
    private RecyclerView recyclerView;
    private SectionListAdapter adapter;
    private String yearPushId = "";
    private Context context;

    private String tag;

    @BindView(R.id.add_sections_fab)
    FloatingActionButton mAddSectionBtn;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sections);
        ButterKnife.bind(this);
        context = this;
        appPrefs = AppPrefs.getInstance();

        Bundle bundle = getIntent().getExtras();

        if (AppPrefs.getInstance().getUserType().equals("HOD")){
            mAddSectionBtn.setVisibility(View.VISIBLE);
        }

        if (bundle != null) {
            yearPushId = bundle.getString("YEAR_PUSH_KEY", "");
            tag = bundle.getString("TAG","");
        }

        //intialize recycler view
        recyclerView = findViewById(R.id.section_rv);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new SectionListAdapter(this,tag);
        recyclerView.setAdapter(adapter);

        mRef = FirebaseDatabase.getInstance().getReference(AppConstants.ATTENDANCE_REFERENCE)
                .child(appPrefs.getCollegeTag())
                .child(AppConstants.TEACHERS_LIST_REFERENCE)
                .child(appPrefs.getFacultyId())
                .child(AppConstants.YEARS_LIST_REFERENCE)
                .child(yearPushId)
                .child(AppConstants.SECTIONS_LIST_REFERENCE);

    }

    @Override
    protected void onStart() {
        super.onStart();
        mRef.addValueEventListener(valueEventListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mRef.removeEventListener(valueEventListener);
    }

    private ValueEventListener valueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if (dataSnapshot.hasChildren()) {
                ArrayList<SectionModel> modelsList = new ArrayList<>();
                Iterable<DataSnapshot> sectionListSnapshot = dataSnapshot.getChildren();
                for (DataSnapshot d1 : sectionListSnapshot) {
                    SectionModel model = d1.getValue(SectionModel.class);
                    if (model != null) {
                        modelsList.add(model);
                    }
                }
                adapter.setSectionData(modelsList);
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    @OnClick(R.id.add_sections_fab)
    public void onViewClicked() {

        Dialog dialog = new Dialog(SectionsActivity.this);
        dialog.setContentView(R.layout.custom_input_dialog_layout);

        TextInputEditText editText = dialog.findViewById(R.id.field_input_dialog_et);
        Button addButton = dialog.findViewById(R.id.dialog_add_btn);
        Button cancelButton = dialog.findViewById(R.id.dialog_cancel_btn);
        TextView textView = dialog.findViewById(R.id.dialog_heading);
        TextInputEditText classTeacherIdTv = dialog.findViewById(R.id.class_teacher_id_dialog_et);
        textView.setText(getResources().getString(R.string.add_section_string));
        editText.setHint(getResources().getString(R.string.section_name_hint_string));
        addButton.setOnClickListener(view -> {
            String sectionName = editText.getEditableText().toString().trim();
            String teacherId = classTeacherIdTv.getEditableText().toString().trim();
            if (sectionName.isEmpty()) {
                editText.setError("Enter valid section name");
                return;
            }
            if (teacherId.isEmpty()) {
                classTeacherIdTv.setError("Enter valid ID");
                return;
            }
            DatabaseReference reference = FirebaseDatabase.getInstance()
                    .getReference(AppConstants.ATTENDANCE_REFERENCE)
                    .child(appPrefs.getCollegeTag())
                    .child(AppConstants.TEACHERS_LIST_REFERENCE);
            reference.child(teacherId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        SectionModel model = new SectionModel();
                        String sectionPushId = mRef.push().getKey();
                        model.setDateOfCreation(AppConstants.getCurrentDateAndTime());
                        model.setFirebasePushId(sectionPushId);
                        model.setSectionName(sectionName);
                        model.setYearFirebaseId(yearPushId);
                        model.setClassTeacherId(teacherId);
                        mRef.child(sectionPushId).setValue(model);
                        dialog.dismiss();

                        String pushId = reference.push().getKey();
                        ClassTeacherModel model1 = new ClassTeacherModel();
                        model1.setYearName(appPrefs.getYearName());
                        model1.setSectionName(sectionName);
                        model1.setFirebasePushId(pushId);
                        reference.child(teacherId)
                                .child(AppConstants.IS_CLASS_TEACHER_OF)
                                .child(pushId)
                                .setValue(model1);
                    } else {
                        AppConstants.showCreationDialog(context);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.e(TAG, "onCancelled: " + databaseError.getMessage());
                }
            });

        });

        cancelButton.setOnClickListener(view -> dialog.dismiss());

        dialog.setCancelable(false);
        dialog.show();
    }


}
