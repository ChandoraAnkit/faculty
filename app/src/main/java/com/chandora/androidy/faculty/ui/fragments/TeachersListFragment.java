package com.chandora.androidy.faculty.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chandora.androidy.faculty.R;
import com.chandora.androidy.faculty.adapters.TeachersListAdapter;
import com.chandora.androidy.faculty.models.Faculty;
import com.chandora.androidy.faculty.utils.AppConstants;
import com.chandora.androidy.faculty.utils.AppPrefs;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link TeachersListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */

// * {@link TeachersListFragment.OnFragmentInteractionListener} interface

public class TeachersListFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
   
    private static final String TAG = TeachersListFragment.class.getSimpleName();
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private Unbinder mBinder;
    private TeachersListAdapter mAdapter;
    private Context mContext;

    private DatabaseReference mTeachersRef;


    @BindView(R.id.teachers_list_rv)
    RecyclerView mTeacherListRv;


//    private OnFragmentInteractionListener mListener;

    public TeachersListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     *
     * @return A new instance of fragment TeachersListFragment.
     */

//    @param param1 Parameter 1.
//            * @param param2 Parameter 2.

    // TODO: Rename and change types and number of parameters
    public static TeachersListFragment newInstance() {
        TeachersListFragment fragment = new TeachersListFragment();
//
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
//        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_teachers_list, container, false);
        mBinder = ButterKnife.bind(this,view);

        return view;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (isAdded()) mBinder.unbind();
    }

    //    // TODO: Rename method, update argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        List<Faculty> teachersList  = new ArrayList<>();

        mAdapter = new TeachersListAdapter();

        mTeachersRef = FirebaseDatabase.getInstance().getReference(AppConstants.ATTENDANCE_REFERENCE)
                    .child(AppPrefs.getInstance().getCollegeTag().toLowerCase())
                    .child(AppConstants.TEACHERS_LIST_REFERENCE);

        mTeachersRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.exists()){

                    for (DataSnapshot snapshot : dataSnapshot.getChildren()){

                        Faculty faculty = snapshot.getValue(Faculty.class);
                        Log.i(TAG, "onDataChange: EXISTS"+faculty.toString());
                        teachersList.add(faculty);
                    }
                }else {

                    Log.i(TAG, "onDataChange: NOT");
                }


                //Set recycler view
                mAdapter.setData(teachersList);
                mTeacherListRv.setLayoutManager(new LinearLayoutManager(mContext));
                mTeacherListRv.setItemAnimator(new DefaultItemAnimator());
                mTeacherListRv.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.i(TAG, "onCancelled: NOt EXIST"+databaseError.getMessage());
            }
            
        });


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }



    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
//    public interface OnFragmentInteractionListener {
//        // TODO: Update argument type and name
//        void onFragmentInteraction(Uri uri);
//    }
}
