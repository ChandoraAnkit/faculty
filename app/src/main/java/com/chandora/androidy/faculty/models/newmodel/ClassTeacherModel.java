package com.chandora.androidy.faculty.models.newmodel;

import com.google.firebase.database.PropertyName;

/**
 * Created by Kalpesh Chandora on 6/10/18.
 */
public class ClassTeacherModel {

    private String yearName;
    private String sectionName;
    private String firebasePushId;

    @PropertyName("year_name")
    public String getYearName() {
        return yearName;
    }

    @PropertyName("year_name")
    public void setYearName(String yearName) {
        this.yearName = yearName;
    }

    @PropertyName("section_name")
    public String getSectionName() {
        return sectionName;
    }

    @PropertyName("section_name")
    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    @PropertyName("firebase_push")
    public String getFirebasePushId() {
        return firebasePushId;
    }

    @PropertyName("firebase_push")
    public void setFirebasePushId(String firebasePushId) {
        this.firebasePushId = firebasePushId;
    }
}
