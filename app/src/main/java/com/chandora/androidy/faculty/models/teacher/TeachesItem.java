package com.chandora.androidy.faculty.models.teacher;

import com.google.firebase.database.PropertyName;

public class TeachesItem {
    private boolean isClassTeacher;
    private int teacherId;

    @PropertyName("is_class_teacher")
    public void setIsClassTeacher(boolean isClassTeacher) {
        this.isClassTeacher = isClassTeacher;
    }

    @PropertyName("is_class_teacher")
    public boolean isIsClassTeacher() {
        return isClassTeacher;
    }

    @PropertyName("teacher_id")
    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }

    @PropertyName("teacher_id")
    public int getTeacherId() {
        return teacherId;
    }

    @Override
    public String toString() {
        return
                "TeachesItem{" +
                        "is_class_teacher = '" + isClassTeacher + '\'' +
                        ",teacher_id = '" + teacherId + '\'' +
                        "}";
    }
}
