package com.chandora.androidy.faculty.models;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.PropertyName;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Kalpesh Chandora on 30/9/18.
 */
public class SectionsListModel {

    public static final String SECTION_NAME = "section_name";
    public static final String SUBJECTS_LIST = "subjects_list";

    private String sectionName;
    private SubjectsListModel subjectsListModel;

    @PropertyName(SECTION_NAME)
    public String getSectionName() {
        return sectionName;
    }

    @PropertyName(SECTION_NAME)
    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    @PropertyName(SUBJECTS_LIST)
    public SubjectsListModel getSubjectsListModel() {
        return subjectsListModel;
    }

    @PropertyName(SUBJECTS_LIST)
    public void setSubjectsListModel(SubjectsListModel subjectsListModel) {
        this.subjectsListModel = subjectsListModel;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put(SECTION_NAME, getSectionName());
        hashMap.put(SUBJECTS_LIST, getSubjectsListModel());
        return hashMap;
    }
}
