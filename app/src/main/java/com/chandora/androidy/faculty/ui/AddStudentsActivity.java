package com.chandora.androidy.faculty.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import com.chandora.androidy.faculty.R;
import com.chandora.androidy.faculty.models.Student;
import com.chandora.androidy.faculty.utils.AppConstants;
import com.chandora.androidy.faculty.utils.AppPrefs;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class AddStudentsActivity extends AppCompatActivity {

    @BindView(R.id.student_id)
    EditText mStudentId;

    @BindView(R.id.student_name)
    EditText mStudentName;

    @BindView(R.id.student_branch)
    EditText mStudentBranch;

    @BindView(R.id.student_section)
    EditText mStudentSection;

    @BindView(R.id.student_contact)
    EditText mStudentContact;

    @BindView(R.id.student_email)
    EditText mStudentEmail;

    private Unbinder binder;

    private DatabaseReference mStudentsRef;
    private AppPrefs mPrefs;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_students);

        getSupportActionBar().setTitle("Add students");

        binder = ButterKnife.bind(this);

        mPrefs = AppPrefs.getInstance();

        mStudentsRef = FirebaseDatabase.getInstance().getReference(AppConstants.ATTENDANCE_REFERENCE)
                .child(mPrefs.getCollegeTag())
                .child(AppConstants.STUDENTS_LIST_REFERENCE);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        binder.unbind();
    }

    @OnClick(R.id.student_submit_btn)
    public void onClickSubmit(){

        Student student = new Student();

        student.setId(mStudentId.getText().toString().trim());
        student.setName(mStudentName.getText().toString().trim());
        student.setBranch(mStudentBranch.getText().toString().trim());
        student.setSection(mStudentSection.getText().toString().trim());
        student.setContact(mStudentContact.getText().toString().trim());
        student.setEmail(mStudentEmail.getText().toString().trim());
        student.setYear(mPrefs.getYearName());
        student.setCollegeTag(mPrefs.getCollegeTag());
        student.setDateOfCreation(AppConstants.getCurrentDateAndTime());

        String modifiedUserName = mPrefs.getCollegeTag() + mStudentId.getText().toString().trim() + "@quickattendance.com";
        student.setGeneratedEmail(modifiedUserName);


        FirebaseAuth.getInstance().createUserWithEmailAndPassword(modifiedUserName,mStudentId.getText().toString().trim())
                .addOnSuccessListener(authResult -> {

                    mStudentsRef.child(mStudentId.getText().toString().trim())
                            .setValue(student);

                })
                .addOnFailureListener(e -> AppConstants.showLongToast(AddStudentsActivity.this, e.getMessage()));



    }
}
