package com.chandora.androidy.faculty.models.teacher;

import com.google.firebase.database.PropertyName;

import java.util.List;

public class SectionsListItem {
    private List<SubjectsListItem> subjectsList;
    private String sectionName;

    @PropertyName("subjects_list")
    public void setSubjectsList(List<SubjectsListItem> subjectsList) {
        this.subjectsList = subjectsList;
    }

    @PropertyName("subjects_list")
    public List<SubjectsListItem> getSubjectsList() {
        return subjectsList;
    }

    @PropertyName("section_name")
    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    @PropertyName("section_name")
    public String getSectionName() {
        return sectionName;
    }

    @Override
    public String toString() {
        return
                "SectionsListItem{" +
                        "subjects_list = '" + subjectsList + '\'' +
                        ",section_name = '" + sectionName + '\'' +
                        "}";
    }
}