package com.chandora.androidy.faculty.models;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.PropertyName;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Kalpesh Chandora on 30/9/18.
 */
public class SubjectsListModel {

    public static final String SUBJECT_NAME = "subject_name";
    public static final String TEACHES = "teaches";

    private String subjectName;
    private TeachersListModel teachersListModel;

    @PropertyName(SUBJECT_NAME)
    public String getSubjectName() {
        return subjectName;
    }

    @PropertyName(SUBJECT_NAME)
    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    @PropertyName(TEACHES)
    public TeachersListModel getTeachersListModel() {
        return teachersListModel;
    }

    @PropertyName(TEACHES)
    public void setTeachersListModel(TeachersListModel teachersListModel) {
        this.teachersListModel = teachersListModel;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put(SUBJECT_NAME, getSubjectName());
        hashMap.put(TEACHES, getTeachersListModel());
        return hashMap;
    }

}
