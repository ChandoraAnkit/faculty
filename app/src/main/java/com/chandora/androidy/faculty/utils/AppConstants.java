package com.chandora.androidy.faculty.utils;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.chandora.androidy.faculty.ui.TeachersActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public final class AppConstants {

    public static final String FIRST_YEAR = "first_year";
    public static final String SECOND_YEAR = "second_year";
    public static final String THIRD_YEAR = "third_year";
    public static final String FOURTH_YEAR = "fourth_year";

    public static final String TEACHERS_LIST_REFERENCE = "teachers_list";
    public static final String STUDENTS_LIST_REFERENCE = "students_list";

    public static final String YEARS_LIST_REFERENCE = "years_list";
    public static final String ATTENDANCE_REFERENCE = "attendance";
    public static final String SECTIONS_LIST_REFERENCE = "sections_list";
    public static final String SUBJECT_LIST_REFERENCE = "subjects_list";
    public static final String TEACHES_SUBJECTS_REFERENCE = "teaches_subjects";
    public static final String IS_CLASS_TEACHER_OF = "is_class_teacher_of";

    public static String getCurrentDateAndTime() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE MMM dd hh:mm aa");
        return simpleDateFormat.format(calendar.getTime());
    }

    public static void showLongToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static void showCreationDialog(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setTitle("Add Teacher")
                .setMessage("The teacher doesn't exists in database. Do you want to add the teacher?")
                .setPositiveButton("Yes", (dialogInterface, i) -> {
                    context.startActivity(new Intent(context, TeachersActivity.class));
                })
                .setNegativeButton("No", (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                });
        builder.setCancelable(false);
        builder.show();
    }
}
