package com.chandora.androidy.faculty.models;

import com.google.firebase.database.PropertyName;

public class Student {

    @PropertyName("id")
    private String id;

    @PropertyName("student_name")
    private String name;

    @PropertyName("student_year")
    private String year;

    @PropertyName("student_branch")
    private String branch;

    @PropertyName("student_section")
    private String section;

    @PropertyName("student_contact")
    private String contact;

    @PropertyName("student_email")
    private String email;

    @PropertyName("college_tag")
    private String collegeTag;

    @PropertyName("college_name")
    private String collegeName;

    @PropertyName("date_of_creation")
    private String dateOfCreation;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCollegeTag() {
        return collegeTag;
    }

    public void setCollegeTag(String collegeTag) {
        this.collegeTag = collegeTag;
    }

    public String getCollegeName() {
        return collegeName;
    }

    public void setCollegeName(String collegeName) {
        this.collegeName = collegeName;
    }

    public String getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(String dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public String getGeneratedEmail() {
        return generatedEmail;
    }

    public void setGeneratedEmail(String generatedEmail) {
        this.generatedEmail = generatedEmail;
    }
}
