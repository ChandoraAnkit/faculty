package com.chandora.androidy.faculty.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.EditText;

import com.chandora.androidy.faculty.R;
import com.chandora.androidy.faculty.models.Faculty;
import com.chandora.androidy.faculty.models.Test;
import com.chandora.androidy.faculty.models.newmodel.YearModel;
import com.chandora.androidy.faculty.utils.AppConstants;
import com.chandora.androidy.faculty.utils.AppPrefs;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ProfileActivity extends AppCompatActivity {

    @BindView(R.id.branch_name_et)
    EditText branchNameEt;

    @BindView(R.id.first_year_cb)
    CheckBox firstYearCb;

    @BindView(R.id.second_year_cb)
    CheckBox secondYearCb;

    @BindView(R.id.third_year_cb)
    CheckBox thirdYearCb;

    @BindView(R.id.fourth_year_cb)
    CheckBox fourthYearCb;

    private Unbinder unbinder;
    private boolean isUpdated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        unbinder = ButterKnife.bind(this);
        isUpdated = false;
    }

    @OnClick(R.id.save_btn)
    public void onClickSaveButton() {

        int count = 0;
        ArrayList<YearModel> yearList = new ArrayList<>();
        String branchName = branchNameEt.getText().toString().trim();
        if (branchName.isEmpty()) {
            branchNameEt.setError("Enter valid branch name");
        } else {
            if (firstYearCb.isChecked()) {
                count++;
                YearModel model = new YearModel();
                model.setDateOfCreation(AppConstants.getCurrentDateAndTime());
                model.setYearName(AppConstants.FIRST_YEAR);
                yearList.add(model);
            }
            if (secondYearCb.isChecked()) {
                count++;
                YearModel model = new YearModel();
                model.setDateOfCreation(AppConstants.getCurrentDateAndTime());
                model.setYearName(AppConstants.SECOND_YEAR);
                yearList.add(model);
            }

            if (thirdYearCb.isChecked()) {
                count++;
                YearModel model = new YearModel();
                model.setDateOfCreation(AppConstants.getCurrentDateAndTime());
                model.setYearName(AppConstants.THIRD_YEAR);
                yearList.add(model);
            }

            if (fourthYearCb.isChecked()) {
                count++;
                YearModel model = new YearModel();
                model.setDateOfCreation(AppConstants.getCurrentDateAndTime());
                model.setYearName(AppConstants.FOURTH_YEAR);
                yearList.add(model);
            }

            DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                    .child(AppConstants.ATTENDANCE_REFERENCE)
                    .child(AppPrefs.getInstance().getCollegeTag())
                    .child(AppConstants.TEACHERS_LIST_REFERENCE)
                    .child(AppPrefs.getInstance().getFacultyId());

            for (int i = 0; i < count; i++) {
                String pushId = ref.child(AppConstants.YEARS_LIST_REFERENCE).push().getKey();
                YearModel model = yearList.get(i);
                model.setFirebasePushId(pushId);
                ref.child(AppConstants.YEARS_LIST_REFERENCE)
                        .child(pushId)
                        .setValue(model);
            }

            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("branch_name", branchName);
            hashMap.put("is_profile_completed", true);
            ref.updateChildren(hashMap);

            startActivity(new Intent(this, MainActivity.class));
            finish();
            isUpdated = true;
        }

    }


    @Override
    protected void onDestroy() {
        if (!isUpdated) {
            FirebaseAuth.getInstance().signOut();
            AppPrefs.getInstance().clearPrefs();
        }
        super.onDestroy();
        unbinder.unbind();
    }
}
