package com.chandora.androidy.faculty.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chandora.androidy.faculty.R;
import com.chandora.androidy.faculty.adapters.YearsListAdapter;
import com.chandora.androidy.faculty.models.newmodel.YearModel;
import com.chandora.androidy.faculty.models.teacher.Response;
import com.chandora.androidy.faculty.ui.LoginActivity;
import com.chandora.androidy.faculty.utils.AppConstants;
import com.chandora.androidy.faculty.utils.AppPrefs;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 */

// * {@link StudentsFragment.OnFragmentInteractionListener} interface

public class StudentsFragment extends Fragment {

    @BindView(R.id.years_rv)
    RecyclerView mYearsRv;

    private DatabaseReference mReference;

    private AppPrefs appPrefs;
    private Unbinder unbinder;

    private ValueEventListener valueEventListener;


    private Context mContext;

    public StudentsFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static StudentsFragment newInstance() {

        StudentsFragment fragment = new StudentsFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_students_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        unbinder = ButterKnife.bind(this,view);

        appPrefs = AppPrefs.getInstance();

        String tag = appPrefs.getCollegeTag();
        String userId = appPrefs.getFacultyId();


        mReference = FirebaseDatabase.getInstance().getReference()
                .child(AppConstants.ATTENDANCE_REFERENCE)
                .child(tag)//    private OnFragmentInteractionListener mListener;

                .child(AppConstants.TEACHERS_LIST_REFERENCE)
                .child(userId);


        if (!TextUtils.isEmpty(tag) && !TextUtils.isEmpty(userId)) {
            valueEventListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    try {

                        Response faculty = dataSnapshot.getValue(Response.class);
                        if (faculty != null) {
                            if (!faculty.isActive()) {

                                showUserDisableDialog();
                                return;
                            }
                            ArrayList<YearModel> arrayList = new ArrayList<>();
                            DataSnapshot yearListSnapshot = dataSnapshot.child(AppConstants.YEARS_LIST_REFERENCE);
                            if (yearListSnapshot.hasChildren()) {
                                Iterable<DataSnapshot> list = yearListSnapshot.getChildren();
                                for (DataSnapshot d1 : list) {

                                    YearModel model = d1.getValue(YearModel.class);
                                    if (model != null) {
                                        arrayList.add(model);
                                    }
                                }
                            }
                            //Setup recycler view
                            mYearsRv.setLayoutManager(new LinearLayoutManager(mContext));
                            mYearsRv.setItemAnimator(new DefaultItemAnimator());
                            mYearsRv.addItemDecoration(new DividerItemDecoration(mContext,DividerItemDecoration.VERTICAL));
                            YearsListAdapter adapter = new YearsListAdapter(mContext,"FRAGMENT");
                            adapter.setData(arrayList);
                            mYearsRv.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            };

            mReference.addListenerForSingleValueEvent(valueEventListener);
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        unbinder.unbind();

        if (valueEventListener != null)
            mReference.removeEventListener(valueEventListener);
    }

    private void showUserDisableDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

        builder.setTitle("Alert!")
                .setMessage("Your account has been disabled temporary! Please contact authority to use our service again.")
                .setCancelable(false)
                .setPositiveButton("OK!", (dialogInterface, i) -> {
                    signOutUser();
                }).show();
    }

    private void signOutUser() {

        FirebaseAuth.getInstance().signOut();
        appPrefs.clearPrefs();
        startActivity(new Intent(mContext, LoginActivity.class));
        getActivity().finish();
    }

}
