package com.chandora.androidy.faculty.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chandora.androidy.faculty.R;
import com.chandora.androidy.faculty.models.newmodel.SectionModel;
import com.chandora.androidy.faculty.ui.StudentsListActivity;
import com.chandora.androidy.faculty.ui.SubjectsActivity;
import com.chandora.androidy.faculty.utils.AppPrefs;

import java.util.ArrayList;

/**
 * Created by Kalpesh Chandora on 2/10/18.
 */
public class SectionListAdapter extends RecyclerView.Adapter<SectionListAdapter.SectionHolder> {

    private Context context;
    private ArrayList<SectionModel> modelArrayList;
    private String tag;

    public SectionListAdapter(Context context,String tag) {
        modelArrayList = new ArrayList<>();
        this.context = context;
        this.tag = tag;
    }

    @NonNull
    @Override
    public SectionHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.section_item_layout, viewGroup, false);
        return new SectionHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SectionHolder sectionHolder, int position) {
        SectionModel model = modelArrayList.get(position);
        sectionHolder.sectionTextView.setText(model.getSectionName());
        sectionHolder.itemView.setOnClickListener(view -> {

            if (tag.equals("ACTIVITY")){

                startSubjectsActivity(model);

            }else if (tag.equals("FRAGMENT")){

                startStudentsActivity(model);

            }


        });
    }

    private void startStudentsActivity(SectionModel model) {

        context.startActivity(new Intent(context,StudentsListActivity.class));
    }

    private void startSubjectsActivity(SectionModel model) {

        Intent intent = new Intent(context, SubjectsActivity.class);
        intent.putExtra("SECTION_PUSH_ID", model.getFirebasePushId());
        intent.putExtra("YEAR_PUSH_ID", model.getYearFirebaseId());
        AppPrefs.getInstance().setSectionName(model.getSectionName());
        context.startActivity(intent);
    }

    public void setSectionData(ArrayList<SectionModel> models) {
        modelArrayList.clear();
        modelArrayList.addAll(models);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    public class SectionHolder extends RecyclerView.ViewHolder {
        TextView sectionTextView;

        public SectionHolder(@NonNull View itemView) {
            super(itemView);
            sectionTextView = itemView.findViewById(R.id.section_item_tv);
        }
    }
}
