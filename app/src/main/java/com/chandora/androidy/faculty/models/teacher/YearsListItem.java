package com.chandora.androidy.faculty.models.teacher;

import com.google.firebase.database.PropertyName;

import java.util.List;

public class YearsListItem {
    private List<SectionsListItem> sectionsList;
    private String yearName;

    @PropertyName("sections_list")
    public void setSectionsList(List<SectionsListItem> sectionsList) {
        this.sectionsList = sectionsList;
    }

    @PropertyName("sections_list")
    public List<SectionsListItem> getSectionsList() {
        return sectionsList;
    }

    @PropertyName("year_name")
    public void setYearName(String yearName) {
        this.yearName = yearName;
    }

    @PropertyName("year_name")
    public String getYearName() {
        return yearName;
    }

    @Override
    public String toString() {
        return
                "YearsListItem{" +
                        "sections_list = '" + sectionsList + '\'' +
                        ",year_name = '" + yearName + '\'' +
                        "}";
    }
}