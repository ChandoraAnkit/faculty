package com.chandora.androidy.faculty.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chandora.androidy.faculty.R;
import com.chandora.androidy.faculty.models.Faculty;
import com.chandora.androidy.faculty.ui.fragments.TeachersListFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ankit
 */
public class TeachersListAdapter extends RecyclerView.Adapter<TeachersListAdapter.TeacherViewHolder> {

    private List<Faculty> list;

    public TeachersListAdapter(){
        list = new ArrayList<>();
    }

    @NonNull
    @Override
    public TeacherViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_teacher_layout,null,false);

        return new TeacherViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TeacherViewHolder viewHolder, int pos) {

        viewHolder.bind(list.get(pos));
    }

    /**
     * Defines to add data to arraylist.
     * @param teachersList list of {@link Faculty}
     */
    public void setData(List<Faculty> teachersList){

        list.clear();
        list.addAll(teachersList);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return list.size() >0 ? list.size() :0;
    }

    static class TeacherViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.tv_teacher_id)
        TextView teacherId;

        @BindView(R.id.tv_teacher_name)
        TextView teacherName;

        public TeacherViewHolder(@NonNull View view) {
            super(view);
            ButterKnife.bind(this,view);
        }


        public void bind(Faculty faculty){

            teacherId.setText(faculty.getUserId());
            teacherName.setText(faculty.getUserFullName());

        }
    }
}
