package com.chandora.androidy.faculty.models.newmodel;

import com.google.firebase.database.PropertyName;

/**
 * Created by Kalpesh Chandora on 2/10/18.
 */
public class YearModel {
    private String yearName;
    private String dateOfCreation;
    private String firebasePushId;

    @PropertyName("year_name")
    public void setYearName(String yearName) {
        this.yearName = yearName;
    }

    @PropertyName("year_name")
    public String getYearName() {
        return yearName;
    }

    @PropertyName("date_of_creation")
    public String getDateOfCreation() {
        return dateOfCreation;
    }

    @PropertyName("date_of_creation")
    public void setDateOfCreation(String dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    @PropertyName("firebase_push_id")
    public String getFirebasePushId() {
        return firebasePushId;
    }

    @PropertyName("firebase_push_id")
    public void setFirebasePushId(String firebasePushId) {
        this.firebasePushId = firebasePushId;
    }
}
