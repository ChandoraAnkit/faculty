package com.chandora.androidy.faculty.models.teacher;

import com.google.firebase.database.PropertyName;

import java.util.List;

public class Response {
    private String collegeTag;
    private String generatedEmail;
    private String userContact;
    private String userId;
    private String userFullName;
    private String userType;
    private String collegeName;
    private String dateOfCreation;
    private String userEmail;
    private String branchName;
    private boolean isActive;
    private boolean isProfileCompleted;
//    private List<YearsListItem> yearsList;

    @PropertyName("is_profile_completed")
    public boolean isProfileCompleted() {
        return isProfileCompleted;
    }

    @PropertyName("is_profile_completed")
    public void setProfileCompleted(boolean profileCompleted) {
        isProfileCompleted = profileCompleted;
    }

    @PropertyName("branch_name")
    public String getBranchName() {
        return branchName;
    }

    @PropertyName("branch_name")
    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    @PropertyName("is_active")
    public boolean isActive() {
        return isActive;
    }

    @PropertyName("is_active")
    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    @PropertyName("user_email")
    public String getUserEmail() {
        return userEmail;
    }

    @PropertyName("user_email")
    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    @PropertyName("date_of_creation")
    public String getDateOfCreation() {
        return dateOfCreation;
    }

    @PropertyName("date_of_creation")
    public void setDateOfCreation(String dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    @PropertyName("college_name")
    public String getCollegeName() {
        return collegeName;
    }

    @PropertyName("college_name")
    public void setCollegeName(String collegeName) {
        this.collegeName = collegeName;
    }

    @PropertyName("college_tag")
    public String getCollegeTag() {
        return collegeTag;
    }

    @PropertyName("college_tag")
    public void setCollegeTag(String collegeTag) {
        this.collegeTag = collegeTag;
    }

    @PropertyName("generated_email")
    public String getGeneratedEmail() {
        return generatedEmail;
    }

    @PropertyName("generated_email")
    public void setGeneratedEmail(String generatedEmail) {
        this.generatedEmail = generatedEmail;
    }

    @PropertyName("user_contact")
    public String getUserContact() {
        return userContact;
    }

    @PropertyName("user_contact")
    public void setUserContact(String userContact) {
        this.userContact = userContact;
    }

    @PropertyName("user_id")
    public String getUserId() {
        return userId;
    }

    @PropertyName("user_id")
    public void setUserId(String userId) {
        this.userId = userId;
    }

    @PropertyName("user_full_name")
    public String getUserFullName() {
        return userFullName;
    }

    @PropertyName("user_full_name")
    public void setUserFullName(String userName) {
        this.userFullName = userName;
    }

    @PropertyName("user_type")
    public String getUserType() {
        return userType;
    }

    @PropertyName("user_type")
    public void setUserType(String userType) {
        this.userType = userType;
    }

//    @PropertyName("years_list")
//    public void setYearsList(List<YearsListItem> yearsList) {
//        this.yearsList = yearsList;
//    }
//
//    @PropertyName("years_list")
//    public List<YearsListItem> getYearsList() {
//        return yearsList;
//    }
//
//    @Override
//    public String toString() {
//        return
//                "Response{" +
//                        "is_profile_completed = '" + isProfileCompleted + '\'' +
//                        ",generated_email = '" + generatedEmail + '\'' +
//                        ",user_email = '" + userEmail + '\'' +
//                        ",is_active = '" + isActive + '\'' +
//                        ",user_type = '" + userType + '\'' +
//                        ",college_name = '" + collegeName + '\'' +
//                        ",user_id = '" + userId + '\'' +
//                        ",user_full_name = '" + userFullName + '\'' +
//                        ",date_of_creation = '" + dateOfCreation + '\'' +
//                        ",college_tag = '" + collegeTag + '\'' +
//                        ",user_contact = '" + userContact + '\'' +
//                        ",years_list = '" + yearsList + '\'' +
//                        "}";
//    }
}