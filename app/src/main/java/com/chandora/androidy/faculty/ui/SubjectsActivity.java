package com.chandora.androidy.faculty.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chandora.androidy.faculty.R;
import com.chandora.androidy.faculty.adapters.SubjectListAdapter;
import com.chandora.androidy.faculty.models.newmodel.ClassTeacherModel;
import com.chandora.androidy.faculty.models.newmodel.SectionModel;
import com.chandora.androidy.faculty.models.newmodel.SubjectsModel;
import com.chandora.androidy.faculty.utils.AppConstants;
import com.chandora.androidy.faculty.utils.AppPrefs;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SubjectsActivity extends AppCompatActivity {

    private DatabaseReference mRef;
    private AppPrefs appPrefs;
    private SubjectListAdapter adapter;
    private RecyclerView subjectsRv;
    private String sectionFirebaseId = "";
    private String yearPushId = "";

    private DatabaseReference mClassTeacherRef;

    @BindView(R.id.add_subjects_fab)
    FloatingActionButton mAddSubjectdsBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subjects);
        ButterKnife.bind(this);
        subjectsRv = findViewById(R.id.subjects_rv);
        subjectsRv.setLayoutManager(new LinearLayoutManager(this));

        appPrefs = AppPrefs.getInstance();

        mClassTeacherRef = FirebaseDatabase.getInstance().getReference(AppConstants.ATTENDANCE_REFERENCE)
                .child(appPrefs.getCollegeTag())
                .child(AppConstants.TEACHERS_LIST_REFERENCE)
                .child(appPrefs.getFacultyId())
                .child("is_class_teacher_of");



        mClassTeacherRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.exists()){

                    if (dataSnapshot.hasChildren()){

                        for (DataSnapshot snapshot : dataSnapshot.getChildren()){

                            ClassTeacherModel classTeacher = snapshot.getValue(ClassTeacherModel.class);

                            if (classTeacher != null && classTeacher.getYearName()
                                    .equals(appPrefs.getYearName())
                                    && classTeacher.getSectionName().
                                    equals(appPrefs.getSectionName())) {

                                mAddSubjectdsBtn.setVisibility(View.VISIBLE);

                            }
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            sectionFirebaseId = bundle.getString("SECTION_PUSH_ID", "");
            yearPushId = bundle.getString("YEAR_PUSH_ID", "");
        }

        adapter = new SubjectListAdapter(this);
        subjectsRv.setAdapter(adapter);

        mRef = FirebaseDatabase.getInstance().getReference(AppConstants.ATTENDANCE_REFERENCE)
                .child(appPrefs.getCollegeTag())
                .child(AppConstants.TEACHERS_LIST_REFERENCE)
                .child(appPrefs.getFacultyId())
                .child(AppConstants.YEARS_LIST_REFERENCE)
                .child(yearPushId)
                .child(AppConstants.SECTIONS_LIST_REFERENCE)
                .child(sectionFirebaseId)
                .child(AppConstants.SUBJECT_LIST_REFERENCE);

    }

    @Override
    protected void onStart() {
        super.onStart();
        mRef.addValueEventListener(valueEventListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mRef.removeEventListener(valueEventListener);
    }

    private ValueEventListener valueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if (dataSnapshot.hasChildren()) {
                ArrayList<SubjectsModel> modelsList = new ArrayList<>();
                Iterable<DataSnapshot> sectionListSnapshot = dataSnapshot.getChildren();
                for (DataSnapshot d1 : sectionListSnapshot) {
                    SubjectsModel model = d1.getValue(SubjectsModel.class);
                    if (model != null) {
                        modelsList.add(model);
                    }
                }
                adapter.setSubjectData(modelsList);
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    @OnClick(R.id.add_subjects_fab)
    public void onViewClicked() {
        Intent intent = new Intent(SubjectsActivity.this, AddSubjectsActivity.class);
        intent.putExtra("SECTION_PUSH_ID", sectionFirebaseId);
        intent.putExtra("YEAR_PUSH_ID", yearPushId);
        startActivity(intent);
    }
}
