package com.chandora.androidy.faculty.models.teacher;

import com.google.firebase.database.PropertyName;

import java.util.List;

public class SubjectsListItem {
    private List<TeachesItem> teaches;
    private String subjectName;

    @PropertyName("teaches")
    public void setTeaches(List<TeachesItem> teaches) {
        this.teaches = teaches;
    }

    @PropertyName("teaches")
    public List<TeachesItem> getTeaches() {
        return teaches;
    }

    @PropertyName("subject_name")
    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    @PropertyName("subject_name")
    public String getSubjectName() {
        return subjectName;
    }

    @Override
    public String toString() {
        return
                "SubjectsListItem{" +
                        "teaches = '" + teaches + '\'' +
                        ",subject_name = '" + subjectName + '\'' +
                        "}";
    }
}