package com.chandora.androidy.faculty.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.chandora.androidy.faculty.R;
import com.chandora.androidy.faculty.models.Faculty;
import com.chandora.androidy.faculty.models.teacher.Response;
import com.chandora.androidy.faculty.utils.AppConstants;
import com.chandora.androidy.faculty.utils.AppPrefs;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;
import java.util.function.BiFunction;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = LoginActivity.class.getSimpleName();

    private FirebaseAuth mFirebaseAuth;
    private DatabaseReference mReference;

    private AppPrefs appPrefs;

    private Unbinder mBinder;

    private String userName;
    private String modifiedUserName;
    private String password;
    private String collegeTag;

    @BindView(R.id.et_username)
    EditText mUserName;

    @BindView(R.id.et_password)
    EditText mPassword;

    @BindView(R.id.et_college_tag)
    EditText mCollegeTag;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mBinder = ButterKnife.bind(this);

        mFirebaseAuth = FirebaseAuth.getInstance();
        mReference = FirebaseDatabase.getInstance().getReference(AppConstants.ATTENDANCE_REFERENCE);

        appPrefs = AppPrefs.getInstance();

        if (mFirebaseAuth.getCurrentUser() != null) openMainActivity();

    }

    @OnClick(R.id.btn_login)
    public void onClickLoginBtn() {

        userName = mUserName.getText().toString().trim();
        password = mPassword.getText().toString().trim();
        collegeTag = mCollegeTag.getText().toString().trim();

        doSignIn(userName, password);

    }

    private void doSignIn(String userName, String password) {

        if (TextUtils.isEmpty(userName)) {
            mUserName.setError(getString(R.string.empty_field_message));
            return;
        }

        if (TextUtils.isEmpty(password)) {
            mPassword.setError(getString(R.string.empty_field_message));
            return;
        }

        if (TextUtils.isEmpty(collegeTag)) {
            mCollegeTag.setError(getString(R.string.empty_field_message));
        }

        if (password.length() < 6) {
            mPassword.setError(getString(R.string.min_pass_len_message));
            return;
        }

        modifiedUserName = collegeTag + userName + "@quickattendance.com";

        mFirebaseAuth.signInWithEmailAndPassword(modifiedUserName, password)

                .addOnSuccessListener(task -> mReference.child(collegeTag)
                        .child("teachers_list")
                        .child(userName)
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                Log.i(TAG, "onDataChange: " + dataSnapshot.getKey() + "-" + dataSnapshot.getValue());
                                try {
                                    Faculty faculty = dataSnapshot.getValue(Faculty.class);
                                    if (faculty != null && faculty.isActive()) {
                                        Toast.makeText(LoginActivity.this, "Successfully login!", Toast.LENGTH_SHORT).show();

                                        appPrefs.setCollegeTag(collegeTag);
                                        appPrefs.setFacultyId(userName);
                                        appPrefs.setUserType(faculty.getUserType());

                                        if (!faculty.isProfileCompleted()) {
                                            openProfileActivity();
                                        } else {
                                            openMainActivity();
                                        }

                                    } else {
                                        FirebaseAuth.getInstance().signOut();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                FirebaseAuth.getInstance().signOut();
                            }
                        }))

                .addOnFailureListener((Exception task) -> {
                    Toast.makeText(this, task.getMessage(), Toast.LENGTH_SHORT).show();
                });

    }

    private void openMainActivity() {
        startActivity(new Intent(LoginActivity.this, MainActivity.class));
        finish();
    }

    private void openProfileActivity() {
        startActivity(new Intent(this, ProfileActivity.class));
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mBinder.unbind();
    }


}
