package com.chandora.androidy.faculty.models;

import com.chandora.androidy.faculty.utils.AppConstants;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.PropertyName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Faculty {

    private String collegeTag;
    private String generatedEmail;
    private String userContact;
    private String userId;
    private String userFullName;
    private String userType;
    private String collegeName;
    private String dateOfCreation;
    private String userEmail;
    private String branchName;
    private boolean isActive;
    private boolean isProfileCompleted;

    @PropertyName("is_profile_completed")
    public boolean isProfileCompleted() {
        return isProfileCompleted;
    }

    @PropertyName("is_profile_completed")
    public void setProfileCompleted(boolean profileCompleted) {
        isProfileCompleted = profileCompleted;
    }

    @PropertyName("branch_name")
    public String getBranchName() {
        return branchName;
    }

    @PropertyName("branch_name")
    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    @PropertyName("is_active")
    public boolean isActive() {
        return isActive;
    }

    @PropertyName("is_active")
    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    @PropertyName("user_email")
    public String getUserEmail() {
        return userEmail;
    }

    @PropertyName("user_email")
    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    @PropertyName("date_of_creation")
    public String getDateOfCreation() {
        return dateOfCreation;
    }

    @PropertyName("date_of_creation")
    public void setDateOfCreation(String dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    @PropertyName("college_name")
    public String getCollegeName() {
        return collegeName;
    }

    @PropertyName("college_name")
    public void setCollegeName(String collegeName) {
        this.collegeName = collegeName;
    }

    @PropertyName("college_tag")
    public String getCollegeTag() {
        return collegeTag;
    }

    @PropertyName("college_tag")
    public void setCollegeTag(String collegeTag) {
        this.collegeTag = collegeTag;
    }

    @PropertyName("generated_email")
    public String getGeneratedEmail() {
        return generatedEmail;
    }

    @PropertyName("generated_email")
    public void setGeneratedEmail(String generatedEmail) {
        this.generatedEmail = generatedEmail;
    }

    @PropertyName("user_contact")
    public String getUserContact() {
        return userContact;
    }

    @PropertyName("user_contact")
    public void setUserContact(String userContact) {
        this.userContact = userContact;
    }

    @PropertyName("user_id")
    public String getUserId() {
        return userId;
    }

    @PropertyName("user_id")
    public void setUserId(String userId) {
        this.userId = userId;
    }

    @PropertyName("user_full_name")
    public String getUserFullName() {
        return userFullName;
    }

    @PropertyName("user_full_name")
    public void setUserFullName(String userName) {
        this.userFullName = userName;
    }

    @PropertyName("user_type")
    public String getUserType() {
        return userType;
    }

    @PropertyName("user_type")
    public void setUserType(String userType) {
        this.userType = userType;
    }


    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("is_profile_completed", isProfileCompleted());
        return hashMap;
    }
}
