package com.chandora.androidy.faculty.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.RadioButton;
import android.widget.Toast;

import com.chandora.androidy.faculty.R;
import com.chandora.androidy.faculty.models.Faculty;
import com.chandora.androidy.faculty.models.newmodel.ClassTeacherModel;
import com.chandora.androidy.faculty.models.newmodel.SectionModel;
import com.chandora.androidy.faculty.models.newmodel.SubjectsModel;
import com.chandora.androidy.faculty.models.newmodel.TeacherSectionModel;
import com.chandora.androidy.faculty.models.newmodel.YearModel;
import com.chandora.androidy.faculty.utils.AppConstants;
import com.chandora.androidy.faculty.utils.AppPrefs;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddSubjectsActivity extends AppCompatActivity {

    private static final String TAG = "AddSubjectsActivity";

    @BindView(R.id.subject_name_et)
    TextInputEditText subjectNameEt;
    @BindView(R.id.teacher_id_et)
    TextInputEditText teacherIdEt;

    private String sectionFirebaseId = "";
    private String yearPushId = "";
    private DatabaseReference mRef;
    private AppPrefs appPrefs;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_subjects);
        ButterKnife.bind(this);
        appPrefs = AppPrefs.getInstance();
        context = AddSubjectsActivity.this;
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            sectionFirebaseId = bundle.getString("SECTION_PUSH_ID", "");
            yearPushId = bundle.getString("YEAR_PUSH_ID", "");
        }
        mRef = customReference(appPrefs.getFacultyId());
    }


    @OnClick(R.id.subject_add_btn)
    public void onViewClicked() {

        String subjectName = subjectNameEt.getEditableText().toString().trim();
        String teacherId = teacherIdEt.getEditableText().toString().trim();
        boolean isClassTeacher;
        if (subjectName.isEmpty()) {
            subjectNameEt.setError("Enter valid name");
            return;
        }
        if (teacherId.isEmpty()) {
            teacherIdEt.setError("Enter valid ID");
            return;
        }

        checkIfExists(teacherId, subjectName);

    }

    private void checkIfExists(String teacherId, String subjectName) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference(AppConstants.ATTENDANCE_REFERENCE)
                .child(appPrefs.getCollegeTag())
                .child(AppConstants.TEACHERS_LIST_REFERENCE);
        ref.child(teacherId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    Faculty faculty = dataSnapshot.getValue(Faculty.class);
                    if (faculty != null && faculty.isActive()) {


//                        This is for class teacher/hod whoever will add subjects
                        String subjectPushId = mRef.push().getKey();
                        SubjectsModel mainModel = getSubjectModel(subjectName, teacherId, subjectPushId);
                        mRef.child(subjectPushId).setValue(mainModel);


                        YearModel model = new YearModel();
                        model.setYearName(appPrefs.getYearName());
                        model.setFirebasePushId(yearPushId);
                        DatabaseReference myNewRef = getYearReference(teacherId);
                        myNewRef.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (dataSnapshot.exists()) {
                                    getIsClassTeacher(teacherId, subjectName);
                                } else {
                                    myNewRef.setValue(model);
                                    getIsClassTeacher(teacherId, subjectName);
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });


                    } else {
                        AppConstants.showLongToast(AddSubjectsActivity.this,
                                "Teacher exists but is not active.");
                    }
                } else {
                    AppConstants.showCreationDialog(context);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void addData(String teacherId, String subjectName, boolean isClassTeacher) {
        TeacherSectionModel sectionModel = new TeacherSectionModel();
        sectionModel.setSectionName(appPrefs.getSectionName());
        sectionModel.setIsClassTeacher(isClassTeacher);
        getYearReference(teacherId)
                .child(AppConstants.SECTIONS_LIST_REFERENCE)
                .child(sectionFirebaseId)
                .setValue(sectionModel);

//                        This is for the teacher who will teach the subject
        DatabaseReference newRef = getYearReference(teacherId)
                .child(AppConstants.SECTIONS_LIST_REFERENCE)
                .child(sectionFirebaseId)
                .child(AppConstants.SUBJECT_LIST_REFERENCE);
        String newPushId = newRef.push().getKey();
        SubjectsModel teacherSubjectModel = getSubjectModel(subjectName, "", newPushId);
        newRef.child(newPushId).setValue(teacherSubjectModel);
        finish();

        AppConstants.showLongToast(context, "Subject added successfully");
    }

    private void getIsClassTeacher(String teacherId, String subjectName) {
        FirebaseDatabase.getInstance().getReference(AppConstants.ATTENDANCE_REFERENCE)
                .child(appPrefs.getCollegeTag())
                .child(AppConstants.TEACHERS_LIST_REFERENCE)
                .child(teacherId)
                .child(AppConstants.IS_CLASS_TEACHER_OF)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists() && dataSnapshot.hasChildren()) {
                            try {
                                Iterable<DataSnapshot> list = dataSnapshot.getChildren();
                                for (DataSnapshot singleData : list) {
                                    ClassTeacherModel model = singleData.getValue(ClassTeacherModel.class);
                                    if (model != null) {
                                        if ((model.getYearName().equals(appPrefs.getYearName()))
                                                && (model.getSectionName().equals(appPrefs.getSectionName()))) {
                                            addData(teacherId, subjectName, true);
                                        }else {
                                            addData(teacherId, subjectName, false);
                                        }
                                    } else {
                                        addData(teacherId, subjectName, false);
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else {
                            addData(teacherId, subjectName, false);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.i(TAG, "onCancelled: " + databaseError.getMessage());
                    }
                });
    }

    private DatabaseReference customReference(String id) {
        return FirebaseDatabase.getInstance().getReference(AppConstants.ATTENDANCE_REFERENCE)
                .child(appPrefs.getCollegeTag())
                .child(AppConstants.TEACHERS_LIST_REFERENCE)
                .child(id)
                .child(AppConstants.YEARS_LIST_REFERENCE)
                .child(yearPushId)
                .child(AppConstants.SECTIONS_LIST_REFERENCE)
                .child(sectionFirebaseId)
                .child(AppConstants.SUBJECT_LIST_REFERENCE);
    }

    private DatabaseReference getYearReference(String id) {
        return FirebaseDatabase.getInstance().getReference(AppConstants.ATTENDANCE_REFERENCE)
                .child(appPrefs.getCollegeTag())
                .child(AppConstants.TEACHERS_LIST_REFERENCE)
                .child(id)
                .child(AppConstants.TEACHES_SUBJECTS_REFERENCE)
                .child(yearPushId);
    }

    private SubjectsModel getSubjectModel(String subjectName,
                                          String teacherId,
                                          String pushId) {
        SubjectsModel model = new SubjectsModel();
        model.setSubjectName(subjectName);
        if (!teacherId.isEmpty()) {
            model.setTeacherId(teacherId);
        }
        model.setFirebasePushId(pushId);
        model.setDateOfCreation(AppConstants.getCurrentDateAndTime());
        return model;
    }
}
