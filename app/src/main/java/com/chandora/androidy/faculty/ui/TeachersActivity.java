package com.chandora.androidy.faculty.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;

import com.chandora.androidy.faculty.R;
import com.chandora.androidy.faculty.models.Faculty;
import com.chandora.androidy.faculty.utils.AppConstants;
import com.chandora.androidy.faculty.utils.AppPrefs;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TeachersActivity extends AppCompatActivity {


    @BindView(R.id.teacher_id_num_et)
    TextInputEditText teacherIdNumEt;
    @BindView(R.id.teacher_full_name_et)
    TextInputEditText teacherFullNameEt;
    @BindView(R.id.teacher_email_et)
    TextInputEditText teacherEmailEt;
    @BindView(R.id.teacher_contact_et)
    TextInputEditText teacherContactEt;

    private DatabaseReference mRef;
    private AppPrefs appPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teachers);
        ButterKnife.bind(this);
        appPrefs = AppPrefs.getInstance();
        mRef = FirebaseDatabase.getInstance().getReference(AppConstants.ATTENDANCE_REFERENCE)
                .child(appPrefs.getCollegeTag())
                .child(AppConstants.TEACHERS_LIST_REFERENCE);
    }

    @OnClick(R.id.create_account_btn)
    public void onViewClicked() {
        String id = teacherIdNumEt.getEditableText().toString().trim();

        Faculty faculty = new Faculty();
        faculty.setDateOfCreation(AppConstants.getCurrentDateAndTime());
        faculty.setCollegeTag(appPrefs.getCollegeTag().toUpperCase());
        faculty.setUserFullName(teacherFullNameEt.getEditableText().toString().trim());
        faculty.setUserEmail(teacherEmailEt.getEditableText().toString().trim());
        faculty.setUserContact(teacherContactEt.getEditableText().toString().trim());
        faculty.setActive(true);
        faculty.setProfileCompleted(true);
        faculty.setUserType("TEACHER");
        faculty.setUserId(id);
        String generatedEmail = appPrefs.getCollegeTag() + id + "@quickattendance.com";
        faculty.setGeneratedEmail(generatedEmail);
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(generatedEmail, id)
                .addOnSuccessListener(authResult -> {
                    AppConstants.showLongToast(TeachersActivity.this, "Account created successfully");
                    mRef.child(id).setValue(faculty);
                    finish();
                })
                .addOnFailureListener(e -> {
                    AppConstants.showLongToast(TeachersActivity.this, e.getMessage());
                });
    }
}
