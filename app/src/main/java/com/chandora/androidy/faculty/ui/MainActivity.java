package com.chandora.androidy.faculty.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.chandora.androidy.faculty.R;
import com.chandora.androidy.faculty.adapters.YearsListAdapter;
import com.chandora.androidy.faculty.models.newmodel.YearModel;
import com.chandora.androidy.faculty.models.teacher.Response;
import com.chandora.androidy.faculty.ui.fragments.StudentsFragment;
import com.chandora.androidy.faculty.ui.fragments.TeachersListFragment;
import com.chandora.androidy.faculty.utils.AppConstants;
import com.chandora.androidy.faculty.utils.AppPrefs;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    @BindView(R.id.years_rv)
    RecyclerView mYearsRv;

    private DatabaseReference mReference;

    private AppPrefs appPrefs;
    private Unbinder unbinder;
    private static final String TAG = "MainActivity";

    private ValueEventListener valueEventListener;

    private FragmentTransaction mFragmentTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        unbinder = ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        appPrefs = AppPrefs.getInstance();

        String tag = appPrefs.getCollegeTag();
        String userId = appPrefs.getFacultyId();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show());


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);




        mReference = FirebaseDatabase.getInstance().getReference()
                .child(AppConstants.ATTENDANCE_REFERENCE)
                .child(tag)
                .child(AppConstants.TEACHERS_LIST_REFERENCE)
                .child(userId);


        if (!TextUtils.isEmpty(tag) && !TextUtils.isEmpty(userId)) {
            valueEventListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Log.i(TAG, "onDataChange: " + dataSnapshot.getKey() + "-" + dataSnapshot.getValue());
                    try {

                        Response faculty = dataSnapshot.getValue(Response.class);
                        if (faculty != null) {
                            if (!faculty.isActive()) {
                                showUserDisableDialog();
                                return;
                            }
                            ArrayList<YearModel> arrayList = new ArrayList<>();
                            DataSnapshot yearListSnapshot = dataSnapshot.child(AppConstants.YEARS_LIST_REFERENCE);
                            if (yearListSnapshot.hasChildren()) {
                                Iterable<DataSnapshot> list = yearListSnapshot.getChildren();
                                for (DataSnapshot d1 : list) {
                                    Log.i(TAG, d1.getKey());
                                    YearModel model = d1.getValue(YearModel.class);
                                    if (model != null) {
                                        arrayList.add(model);
                                    }
                                }
                            }
                            //Setup recycler view
                            mYearsRv.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                            mYearsRv.setItemAnimator(new DefaultItemAnimator());
                            YearsListAdapter adapter = new YearsListAdapter(MainActivity.this,"ACTIVITY");
                            adapter.setData(arrayList);
                            mYearsRv.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            };

            mReference.addListenerForSingleValueEvent(valueEventListener);
        }

    }


    @Override
    protected void onDestroy() {

        if (valueEventListener != null)
            mReference.removeEventListener(valueEventListener);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.sign_out_menu) {
            signOutUser();
        }

        return super.onOptionsItemSelected(item);
    }

    private void signOutUser() {

        FirebaseAuth.getInstance().signOut();
        appPrefs.clearPrefs();
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_teachers_list) {

            doFragmentTransaction(TeachersListFragment.newInstance());

        } else if (id == R.id.nav_students_list) {

            doFragmentTransaction(StudentsFragment.newInstance());

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void doFragmentTransaction(Fragment fragment) {

        //  Begin transaction
        mFragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mFragmentTransaction.replace(R.id.main_content,fragment);

        //save fragment state
        mFragmentTransaction.addToBackStack(null);

        //commit the transaction
        mFragmentTransaction.commit();
    }


    private void showUserDisableDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Alert!")
                .setMessage("Your account has been disabled temporary! Please contact authority to use our service again.")
                .setCancelable(false)
                .setPositiveButton("OK!", (dialogInterface, i) -> {
                    signOutUser();
                }).show();
    }
}
