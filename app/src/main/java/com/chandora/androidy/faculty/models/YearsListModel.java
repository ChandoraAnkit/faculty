package com.chandora.androidy.faculty.models;

import com.google.firebase.database.PropertyName;

/**
 * Created by Kalpesh Chandora on 30/9/18.
 */
public class YearsListModel {

    public static final String YEAR_NAME = "year_name";
    public static final String SECTIONS_LIST = "sections_list";

    private SectionsListModel sectionsListModel;

    @PropertyName(SECTIONS_LIST)
    public SectionsListModel getSectionsListModel() {
        return sectionsListModel;
    }

    @PropertyName(SECTIONS_LIST)
    public void setSectionsListModel(SectionsListModel sectionsListModel) {
        this.sectionsListModel = sectionsListModel;
    }
}
