package com.chandora.androidy.faculty.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.chandora.androidy.faculty.MyApplication;

public class AppPrefs {

    private final static String COLLEGE_TAG = "COLLEGE_TAG";
    private final static String FACULTY_ID = "FACULTY_ID_TAG";
    private final static String YEAR_NAME = "YEAR_NAME_TAG";
    private final static String SECTION_NAME = "SECTION_NAME_TAG";
    private final static  String USER_TYPE = "USER_TYPE";

    private static volatile AppPrefs INSTANCE;
    private final SharedPreferences prefs;

    public static AppPrefs getInstance() {
        if (INSTANCE == null) {
            synchronized (AppPrefs.class) {
                INSTANCE = new AppPrefs(MyApplication.getAppContext());
            }
        }
        return INSTANCE;
    }

    private AppPrefs(Context context) {
        prefs = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
    }

    public void setCollegeTag(String tag) {
        prefs.edit().putString(COLLEGE_TAG, tag).apply();
    }

    public String getCollegeTag() {
        return prefs.getString(COLLEGE_TAG, "");
    }

    public void setFacultyId(String id) {
        prefs.edit().putString(FACULTY_ID, id).apply();
    }

    public String getFacultyId() {
        return prefs.getString(FACULTY_ID, "");
    }

    public void clearPrefs() {
        prefs.edit().clear().apply();
    }

    public void setYearName(String yearName) {
        prefs.edit().putString(YEAR_NAME, yearName).apply();
    }

    public String getYearName() {
        return prefs.getString(YEAR_NAME, "");
    }

    public void setSectionName(String sectionName) {
        prefs.edit().putString(SECTION_NAME, sectionName).apply();
    }

    public String getSectionName() {
        return prefs.getString(SECTION_NAME, "");
    }

    public String getUserType(){return  prefs.getString(USER_TYPE,"");}

    public void setUserType(String userType){ prefs.edit().putString(USER_TYPE,userType).apply();}
}
