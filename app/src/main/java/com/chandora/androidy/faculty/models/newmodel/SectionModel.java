package com.chandora.androidy.faculty.models.newmodel;

import com.google.firebase.database.PropertyName;

/**
 * Created by Kalpesh Chandora on 2/10/18.
 */
public class SectionModel {

    private String sectionName;
    private String dateOfCreation;
    private String firebasePushId;
    private String yearFirebaseId;
    private String classTeacherId;

    @PropertyName("section_name")
    public String getSectionName() {
        return sectionName;
    }

    @PropertyName("section_name")
    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    @PropertyName("date_of_creation")
    public String getDateOfCreation() {
        return dateOfCreation;
    }

    @PropertyName("date_of_creation")
    public void setDateOfCreation(String dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    @PropertyName("firebase_push_id")
    public String getFirebasePushId() {
        return firebasePushId;
    }

    @PropertyName("firebase_push_id")
    public void setFirebasePushId(String firebasePushId) {
        this.firebasePushId = firebasePushId;
    }

    @PropertyName("year_push_id")
    public String getYearFirebaseId() {
        return yearFirebaseId;
    }

    @PropertyName("year_push_id")
    public void setYearFirebaseId(String yearFirebaseId) {
        this.yearFirebaseId = yearFirebaseId;
    }

    @PropertyName("class_teacher_id")
    public String getClassTeacherId() {
        return classTeacherId;
    }

    @PropertyName("class_teacher_id")
    public void setClassTeacherId(String classTeacherId) {
        this.classTeacherId = classTeacherId;
    }
}
