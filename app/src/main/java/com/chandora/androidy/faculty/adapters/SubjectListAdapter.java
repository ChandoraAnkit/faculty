package com.chandora.androidy.faculty.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chandora.androidy.faculty.R;
import com.chandora.androidy.faculty.models.newmodel.SubjectsModel;

import java.util.ArrayList;

/**
 * Created by Kalpesh Chandora on 2/10/18.
 */
public class SubjectListAdapter extends RecyclerView.Adapter<SubjectListAdapter.SubjectHolder> {

    private Context context;
    private ArrayList<SubjectsModel> subjectsModels;

    public SubjectListAdapter(Context context) {
        this.context = context;
        subjectsModels = new ArrayList<>();
    }

    @NonNull
    @Override
    public SubjectHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.subject_item_layout, parent, false);
        return new SubjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SubjectHolder holder, int position) {
        holder.subjectNameTv.setText(subjectsModels.get(position).getSubjectName());
        holder.teacherNameTv.setText(subjectsModels.get(position).getTeacherId());
    }

    @Override
    public int getItemCount() {
        return subjectsModels.size();
    }

    public class SubjectHolder extends RecyclerView.ViewHolder {
        TextView subjectNameTv;
        TextView teacherNameTv;

        public SubjectHolder(@NonNull View itemView) {
            super(itemView);
            subjectNameTv = itemView.findViewById(R.id.subject_name_tv);
            teacherNameTv = itemView.findViewById(R.id.teacher_name_tv);
        }
    }

    public void setSubjectData(ArrayList<SubjectsModel> models) {
        subjectsModels.clear();
        subjectsModels.addAll(models);
        notifyDataSetChanged();
    }
}
