package com.chandora.androidy.faculty.models.newmodel;

import com.google.firebase.database.PropertyName;

/**
 * Created by Kalpesh Chandora on 6/10/18.
 */
public class TeacherSectionModel {
    private String sectionName;
    private String yearName;
    private boolean isClassTeacher;

    @PropertyName("section_name")
    public String getSectionName() {
        return sectionName;
    }

    @PropertyName("section_name")
    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    @PropertyName("year_name")
    public String getYearName() {
        return yearName;
    }

    @PropertyName("year_name")
    public void setYearName(String yearName) {
        this.yearName = yearName;
    }

    @PropertyName("is_class_teacher")
    public boolean getIsClassTeacher() {
        return isClassTeacher;
    }

    @PropertyName("is_class_teacher")
    public void setIsClassTeacher(boolean isClassTeacher) {
        this.isClassTeacher = isClassTeacher;
    }
}
