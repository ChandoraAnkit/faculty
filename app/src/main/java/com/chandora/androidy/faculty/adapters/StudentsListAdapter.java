package com.chandora.androidy.faculty.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chandora.androidy.faculty.R;
import com.chandora.androidy.faculty.models.Student;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StudentsListAdapter extends RecyclerView.Adapter<StudentsListAdapter.StudentHolder> {

    private List<Student> studentsList;

    public StudentsListAdapter(){
        studentsList =  new ArrayList<>();
    }

    @NonNull
    @Override
    public StudentHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.student_layout,parent,false);
        return new StudentHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StudentHolder viewHolder, int pos) {

        viewHolder.bind(studentsList.get(pos));
    }

    public void setData(List<Student> list){

        studentsList.clear();
        studentsList.addAll(list);

    }

    @Override
    public int getItemCount() {
        return studentsList.size() <0 ?0:studentsList.size();
    }

    static class StudentHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.student_name_tv)
        TextView studentName;

        public StudentHolder(@NonNull View view) {
            super(view);
            ButterKnife.bind(this,view);
        }

        public void bind(Student student){

            studentName.setText(student.getName());
        }
    }
}
