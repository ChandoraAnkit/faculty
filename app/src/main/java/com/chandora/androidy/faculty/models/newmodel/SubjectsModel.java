package com.chandora.androidy.faculty.models.newmodel;

import com.google.firebase.database.PropertyName;

/**
 * Created by Kalpesh Chandora on 2/10/18.
 */
public class SubjectsModel {

    private String subjectName;
    private String teacherId;
    private String dateOfCreation;
    private String firebasePushId;

    @PropertyName("subject_name")
    public String getSubjectName() {
        return subjectName;
    }

    @PropertyName("subject_name")
    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    @PropertyName("teacher_id")
    public String getTeacherId() {
        return teacherId;
    }

    @PropertyName("teacher_id")
    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    @PropertyName("date_of_creation")
    public String getDateOfCreation() {
        return dateOfCreation;
    }

    @PropertyName("date_of_creation")
    public void setDateOfCreation(String dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    @PropertyName("firebase_push_id")
    public String getFirebasePushId() {
        return firebasePushId;
    }

    @PropertyName("firebase_push_id")
    public void setFirebasePushId(String firebasePushId) {
        this.firebasePushId = firebasePushId;
    }
}
