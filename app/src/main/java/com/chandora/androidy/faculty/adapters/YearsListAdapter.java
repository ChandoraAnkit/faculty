package com.chandora.androidy.faculty.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chandora.androidy.faculty.R;
import com.chandora.androidy.faculty.models.newmodel.YearModel;
import com.chandora.androidy.faculty.ui.SectionsActivity;
import com.chandora.androidy.faculty.utils.AppPrefs;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class YearsListAdapter extends RecyclerView.Adapter<YearsListAdapter.YearViewHolder> {

    private ArrayList<YearModel> list;
    private Context context;
    private String tag;


    public YearsListAdapter(Context context,String tag) {
        list = new ArrayList<>();
        this.context = context;
        this.tag = tag;

    }

    @NonNull
    @Override
    public YearViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_year_layout, parent, false);
        return new YearViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull YearViewHolder holder, int pos) {
        holder.bind(list.get(pos).getYearName());
        holder.itemView.setOnClickListener(view -> {

            Intent intent = new Intent(context, SectionsActivity.class);
            intent.putExtra("YEAR_PUSH_KEY", list.get(pos).getFirebasePushId());
            intent.putExtra("TAG",tag);
            AppPrefs.getInstance().setYearName(list.get(pos).getYearName());
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return list.size() > 0 ? list.size() : 0;
    }

    static class YearViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.year_tv)
        TextView yearTv;

        public YearViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(String year) {
            yearTv.setText(year);
        }
    }

    public void setData(ArrayList<YearModel> yearsList) {
        if (list != null) {
            list.clear();
            list.addAll(yearsList);
        }
    }
}
