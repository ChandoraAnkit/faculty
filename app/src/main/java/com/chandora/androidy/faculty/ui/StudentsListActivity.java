package com.chandora.androidy.faculty.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.chandora.androidy.faculty.R;
import com.chandora.androidy.faculty.adapters.StudentsListAdapter;
import com.chandora.androidy.faculty.models.Faculty;
import com.chandora.androidy.faculty.models.Student;
import com.chandora.androidy.faculty.models.newmodel.ClassTeacherModel;
import com.chandora.androidy.faculty.utils.AppConstants;
import com.chandora.androidy.faculty.utils.AppPrefs;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class StudentsListActivity extends AppCompatActivity {

    private final static String TAG = StudentsListActivity.class.getSimpleName();

    private AppPrefs mAppPrefs;
    private Unbinder binder;

    private DatabaseReference mCollegeRef;
    private DatabaseReference mFacultyRef;
    private DatabaseReference mStudentRef;

    private List<Student> studentsList;

    @BindView(R.id.students_add_fab)
    FloatingActionButton mStudentsAddBtn;

    @BindView(R.id.students_list_rv)
    RecyclerView mStudentsRv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_students_list);

        getSupportActionBar().setTitle("Students");

        binder = ButterKnife.bind(this);

        mAppPrefs = AppPrefs.getInstance();
        studentsList = new ArrayList<>();

        mCollegeRef = FirebaseDatabase.getInstance().getReference(AppConstants.ATTENDANCE_REFERENCE)
                .child(mAppPrefs.getCollegeTag());


        mFacultyRef = mCollegeRef.child(AppConstants.TEACHERS_LIST_REFERENCE)
                .child(mAppPrefs.getFacultyId())
                .child(AppConstants.IS_CLASS_TEACHER_OF);


        mFacultyRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.exists()) {
                    if (dataSnapshot.hasChildren()) {

                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            ClassTeacherModel teacher = snapshot.getValue(ClassTeacherModel.class);

                            if (teacher.getSectionName().equals(mAppPrefs.getSectionName())
                                    && teacher.getYearName().equals(mAppPrefs.getYearName())) {
                                mStudentsAddBtn.setVisibility(View.VISIBLE);
                            }

                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mStudentRef = mCollegeRef.child(AppConstants.STUDENTS_LIST_REFERENCE);

        mStudentRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.exists()) {
                    if (dataSnapshot.hasChildren()) {

                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Student student = snapshot.getValue(Student.class);
                            studentsList.add(student);
                        }

                        //Setup adapter
                        StudentsListAdapter adapter = new StudentsListAdapter();
                        adapter.setData(studentsList);

                        //setup recycler view

                        mStudentsRv.setLayoutManager(new LinearLayoutManager(StudentsListActivity.this));
                        mStudentsRv.setItemAnimator(new DefaultItemAnimator());
                        mStudentsRv.setAdapter(adapter);

                        adapter.notifyDataSetChanged();




                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        binder.unbind();
    }

    @OnClick(R.id.students_add_fab)
    public void onClickAddButton() {

        startActivity(new Intent(this, AddStudentsActivity.class));
    }


}
